package main

import (
    "fmt"
    "io"
    "os"
)

func main() {

    const var1, var2 = "learning", "fmt.Sprintf()"

    s := fmt.Sprintf("currently %s how to use %s", var1, var2)

    io.WriteString(os.Stdout, s)


}
